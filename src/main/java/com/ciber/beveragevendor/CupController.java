package com.ciber.beveragevendor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;

/***
 * Controller returns a serving of lemonade as in JSON
 *
 */
@Slf4j
@RestController
@RequestMapping("/cup")
public class CupController {

    private final Counter cupCounter;
    private final Counter cupCost;

    public CupController(MeterRegistry registry) {
        this.cupCounter = registry.counter(this.getClass().getPackage().getName() + ".cups.delivered");
        this.cupCost = registry.counter(this.getClass().getPackage().getName() + ".cups.cost");
    }

    @GetMapping("/{size}")
    public ServingCup getServing(@PathVariable String size) {
        ServingCup cup = new ServingCup(size);
        log.info("ServingCup {}", cup);

        cupCounter.increment();
        cupCost.increment(cup.getCostCents());

        return cup;
    }
}
