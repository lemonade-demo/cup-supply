package com.ciber.beveragevendor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

/***
 * Simplest Lemonade Supply Service
 * 
 * @author Mark
 *
 *         Provides a serving of lemonade
 */
@Slf4j
@SpringBootApplication
public class CupSupplyService {

    public static void main(String[] args) {
        log.info("start - {}", System.getProperty("user.name"));
        System.setProperty("server.port", "9001");
        System.setProperty("spring.output.ansi.enabled", "never");
        System.setProperty("management.endpoints.jmx.exposure.exclude", "*");
        System.setProperty("management.endpoints.web.exposure.include", "info,prometheus,health,env,beans");

        log.info("run");
        SpringApplication.run(CupSupplyService.class, args);
    }

}
